/* eslint-disable prettier/prettier */
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common';
import { AppService } from './app.service';
import { ClassStudent } from './Entities/ClassStudent';
import { Student } from './Entities/Student';
import { AuthGuard } from './auth/auth.guard';

@UseGuards(AuthGuard)
@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  // Controller ClassStudent
  @Get('/class-students')
  async getAllClassStudent(): Promise<ClassStudent[]> {
    return await this.appService.getAllClassStudent();
  }

  @Post('/class-students')
  async createClassStudent(
    @Body() classStudentData: Partial<ClassStudent>,
  ): Promise<ClassStudent> {
    return await this.appService.createClassStudent(classStudentData);
  }

  @Get('/class-students/:id')
  async getClassStudentById(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<ClassStudent> {
    return await this.appService.getClassStudentRelation(id);
  }

  @Put('/class-students/:id')
  async updateClassStudent(
    @Param('id', ParseIntPipe) id: number,
    @Body() classStudentData: Partial<ClassStudent>,
  ): Promise<ClassStudent> {
    return await this.appService.updateClassStudent(id, classStudentData);
  }

  @Delete('/class-students/:id')
  async deleteClassStudent(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<void> {
    return await this.appService.deleteClassStudent(id);
  }

  @Get('/class-students-search')
  async getClassStudentByName(
    @Query('name') name: string,
  ): Promise<ClassStudent[]> {
    return await this.appService.findClassStudentName(name);
  }

  // Controller Student
  @Post('/students')
  async createStudent(@Body() studentData: Partial<Student>): Promise<Student> {
    return await this.appService.createStudent(studentData);
  }

  @Get('/students')
  async getAllStudent(): Promise<Student[]> {
    return await this.appService.getAllStudent();
  }

  @Get('/students/:id')
  async getStudentById(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<Student> {
    return await this.appService.getStudentByIdRelation(id);
  }

  @Put('/students/:id')
  async updateStudent(
    @Param('id', ParseIntPipe) id: number,
    @Body() updateStudentData: Partial<Student>,
  ): Promise<Student> {
    return await this.appService.updateStudent(id, updateStudentData);
  }

  @Delete('/students/:id')
  async deleteStudent(@Param('id', ParseIntPipe) id: number): Promise<void> {
    await this.appService.deleteStudent(id);
  }

  @Get('/students-name')
  async findStudentByName(@Query('search') search: string): Promise<Student[]> {
    return await this.appService.findStudentByName(search);
  }
}
