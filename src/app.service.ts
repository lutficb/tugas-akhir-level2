/* eslint-disable prettier/prettier */
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ClassStudent } from './Entities/ClassStudent';
import { Repository } from 'typeorm';
import { Student } from './Entities/Student';
// import { updateStudentDto } from './Dto/StudentDto';

@Injectable()
export class AppService {
  constructor(
    @InjectRepository(ClassStudent)
    private readonly classStudentRepository: Repository<ClassStudent>,

    @InjectRepository(Student)
    private readonly studentRepository: Repository<Student>,
  ) {}

  // Class Student Service
  async createClassStudent(
    classStudentData: Partial<ClassStudent>,
  ): Promise<ClassStudent> {
    const classStudent = this.classStudentRepository.create(classStudentData);
    return await this.classStudentRepository.save(classStudent);
  }

  async getAllClassStudent(): Promise<ClassStudent[]> {
    return await this.classStudentRepository.find({ relations: ['students'] });
  }

  async getClassStudentById(id: number): Promise<ClassStudent> {
    return await this.classStudentRepository.findOneBy({ id: id });
  }

  async updateClassStudent(
    id: number,
    classStudentData: Partial<ClassStudent>,
  ): Promise<ClassStudent> {
    await this.classStudentRepository.update(id, classStudentData);
    return this.getClassStudentById(id);
  }

  async getClassStudentRelation(id: number): Promise<ClassStudent> {
    const classStudent = await this.classStudentRepository.findOne({
      where: { id: id },
      relations: { students: true },
    });
    if (!classStudent)
      throw new HttpException(
        `Class student dengan id ${id} tidak ditemukan`,
        HttpStatus.BAD_REQUEST,
      );
    return classStudent;
  }

  async deleteClassStudent(id: number): Promise<void> {
    const classStudent = await this.classStudentRepository.findOneBy({
      id: id,
    });
    if (!classStudent)
      throw new HttpException(
        'Data tidak ditemukan. Proses delete gagal',
        HttpStatus.BAD_REQUEST,
      );
    this.classStudentRepository.delete(id);
  }

  // Filter pencarian class student dengan nama
  async findClassStudentName(name: string): Promise<ClassStudent[]> {
    const classStudentName = await this.classStudentRepository
      .createQueryBuilder()
      .select('class')
      .from(ClassStudent, 'class')
      .where('class.className like :className', {
        className: `%${name}%`,
      })
      .getMany();
    if (classStudentName.length == 0)
      throw new HttpException(
        `Tidak ditemukan data dengan kata kunci = ${name}`,
        HttpStatus.BAD_REQUEST,
      );
    return classStudentName;
  }

  // Student Services
  async createStudent(studentData: Partial<Student>): Promise<Student> {
    const newStudent = await this.studentRepository.create(studentData);
    return await this.studentRepository.save(newStudent);
  }

  async getAllStudent(): Promise<Student[]> {
    return await this.studentRepository.find({ relations: ['classStudent'] });
  }

  async getStudentById(id: number): Promise<Student> {
    return await this.studentRepository.findOneBy({ id: id });
  }

  async getStudentByIdRelation(id: number): Promise<Student> {
    const student = await this.studentRepository.findOne({
      where: { id: id },
      relations: { classStudent: true },
    });
    if (!student)
      throw new HttpException(
        `Student dengan id ${id} tidak ditemukan`,
        HttpStatus.BAD_REQUEST,
      );
    return student;
  }

  async updateStudent(
    id: number,
    updateStudentData: Partial<Student>,
  ): Promise<Student> {
    await this.studentRepository.update(id, updateStudentData);
    return this.getStudentById(id);
  }

  async deleteStudent(id: number): Promise<void> {
    await this.studentRepository.delete(id);
  }

  // Filter pencarian student berdasarkan nama
  async findStudentByName(name: string): Promise<Student[]> {
    const student = await this.studentRepository
      .createQueryBuilder()
      .select('students')
      .from(Student, 'students')
      .where('students.name like :name', { name: `%${name}%` })
      .getMany();
    if (student.length == 0)
      throw new HttpException(
        `Tidak ditemukan hasil pencarian dengan kata kunci ${name}`,
        HttpStatus.BAD_REQUEST,
      );
    return await student;
  }
}
