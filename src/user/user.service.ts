/* eslint-disable prettier/prettier */
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/Entities/User';
import { Repository } from 'typeorm';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {}

  async getUserByEmail(email: string): Promise<User> {
    return await this.userRepository.findOne({ where: [{ email: email }] });
  }

  async createUser(userDetailData: Partial<User>): Promise<User> {
    const newUser = this.userRepository.create(userDetailData);
    return await this.userRepository.save(newUser);
  }
}
