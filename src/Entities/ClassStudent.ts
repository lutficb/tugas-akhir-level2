/* eslint-disable prettier/prettier */
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Student } from './Student';

@Entity({ name: 'class_student' })
export class ClassStudent {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  className: string;

  @OneToMany(() => Student, (student) => student.classStudent)
  students: Student[];
}
