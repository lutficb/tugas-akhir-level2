/* eslint-disable prettier/prettier */
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Student } from './Entities/Student';
import { ClassStudent } from './Entities/ClassStudent';
import { User } from './Entities/User';
import { AuthModule } from './auth/auth.module';
import { UserModule } from './user/user.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mariadb',
      host: 'localhost',
      port: 3307,
      username: 'root',
      password: 'root',
      database: 'tugas-akhir-level2',
      entities: [Student, ClassStudent, User],
      synchronize: true,
    }),
    TypeOrmModule.forFeature([Student, ClassStudent]),
    AuthModule,
    UserModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
